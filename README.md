
# Documentation des fonctions : #
# Commande #

#### Method GestionCommande.controleur.CommandeControleur.CreerCommande(GestionCommande.model.Client,System.Collections.Generic.ICollection{GestionCommande.model.LigneCommande})

 Creer une fonction 

|Name | Description |
|-----|------|
|client: |Objet Client|
|ligneCmd: |Objet Ligne de commande|


---
#### Method GestionCommande.controleur.CommandeControleur.CreerClient(GestionCommande.model.Client)

 Creer un client 

|Name | Description |
|-----|------|
|client: |Objet Client|


---
#### Method GestionCommande.controleur.CommandeControleur.CreerProduit(GestionCommande.model.Produit)

 Creer un produit 

|Name | Description |
|-----|------|
|produit: |Objet Produit|


---
#### Method GestionCommande.controleur.CommandeControleur.GetClients

 Obtenir la liste des clients 

**Returns**: Collection de client via l'objet ClientDao.Clients



---
#### Method GestionCommande.controleur.CommandeControleur.GetProduits

 Obtenir la liste des produits 

**Returns**: Collection de produit via l'objet ProduitDao.Produits



---
#### Method GestionCommande.controleur.CommandeControleur.GetCommandes

 Obtenir la liste des commandes 

**Returns**: Collection de commande via l'objet CommandeDao.Commandes



---
#### Method GestionCommande.dao.ClientDao.AjouterClient(GestionCommande.model.Client)

 Ajout d'un client dans l'attribut Clients de la classe ClientDao 

|Name | Description |
|-----|------|
|client: |Objet Client|


---
#### Method GestionCommande.dao.CommandeDao.AjouterCommande(GestionCommande.model.Commande)

 Ajout d'une commande dans l'attribut Commandes de la classe CommandeDao 

|Name | Description |
|-----|------|
|commande: |Objet model.Commande|


---
#### Method GestionCommande.dao.ProduitDao.Ajouter(GestionCommande.model.Produit)

 Ajout d'un Produit dans la classe ProduitDao 

|Name | Description |
|-----|------|
|produit: |Objet Produit|


---
#### Method GestionCommande.vue.MainVue.AfficherCommandeParClient

 Affiche les Commande Par Client 



---
#### Method GestionCommande.vue.MainVue.AfficherCommandes

 Appel l'affichage de la liste de toutes les commandes 



---
#### Method GestionCommande.vue.MainVue.AfficherCommandes(System.Collections.Generic.ICollection{GestionCommande.model.Commande})

 Gere l'affichage des commandes 

|Name | Description |
|-----|------|
|commandes: ||


---
#### Method GestionCommande.vue.MainVue.AfficherProduits

 Gere l'affichage des produit 



---
#### Method GestionCommande.vue.MainVue.AjouterCommande

 Interface d'ajout de commande fait appel au controleur pour la gestion de l'ajout de la commande 



---
#### Method GestionCommande.vue.MainVue.CreateProduit

 Interface de creation des produit fait appel au controleur pour la gestion de l'ajout du produit 



---
#### Method GestionCommande.vue.MainVue.AjouterClient

 Gestion de l'interface d'ajout de client, fait appel au controlleur pour gerer l'ajout du client 



---
#### Method GestionCommande.vue.MainVue.ChoisirClient

 Interface de selection d'un client a partir de son id 

**Returns**: GestionModel.Client Client



---
#### Method GestionCommande.vue.MainVue.AjouterLignes

 Interface d'ajout de ligne dans une commande 

**Returns**: Collection de ligne de commandes



---
#### Method GestionCommande.vue.MainVue.AjouterProduit

 Permet d'ajouter un produit à une commande 

**Returns**: retourne un produit



---
#### Method GestionCommande.vue.MainVue.AjouterQuantité

 Ajout d'une quantié à commande 

**Returns**: Int quantité



---
#### Method GestionCommande.vue.MainVue.AfficherClients

 Affiche les clients par id prenom et nom 



---
## Type TestAjoutClient

 Description résumée pour TestAjoutClient 



---
#### Property TestAjoutClient.TestContext

 Obtient ou définit le contexte de test qui fournit des informations sur la série de tests active, ainsi que ses fonctionnalités. 



---
#### Method TestAjoutClient.TestProduit

 Test l'ajout d'un produit 



---
#### Method TestAjoutClient.TestAjoutCommandeOk

 Test l'ajout d'une commande 



---
#### Method TestAjoutClient.TestClient

 Test L'ajout d'un client 



---


