﻿using System.Collections.Generic;
using GestionCommande.model;
using System.Collections.ObjectModel;
namespace GestionCommande.dao
{
    class ClientDao
    {
        public ICollection<Client> Clients { get; }

        public ClientDao()
        {
            this.Clients = new Collection<Client>();
            this.Clients.Add(new Client() { Id = 1, Nom = "Banner", Prenom = "Bruce", Mail = "hulk@gmail.com", Commandes = new Collection<GestionCommande.model.Commande>() });
            this.Clients.Add(new Client() { Id = 2, Nom = "Stark", Prenom = "Tony", Mail = "tony.stark@gmail.com", Commandes = new Collection<GestionCommande.model.Commande>() });
            this.Clients.Add(new Client() { Id = 3, Nom = "", Prenom = "Thor", Mail = "thor@gmail.com",Commandes = new Collection<GestionCommande.model.Commande>() });
        }
        /// <summary>
        /// Ajout d'un client dans l'attribut Clients de la classe ClientDao
        /// </summary>
        /// <param name="client">Objet Client</param>
        public void AjouterClient(Client client)
        {
            this.Clients.Add(client);
        }
    }
}
