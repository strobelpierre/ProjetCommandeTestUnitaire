﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.ObjectModel;
using System.Linq;

namespace Commande
{
    /// <summary>
    /// Description résumée pour TestAjoutClient
    /// </summary>
    [TestClass]
    public class TestAjoutClient
    {
        public TestAjoutClient()
        {
            //
            // TODO: ajoutez ici la logique du constructeur
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Obtient ou définit le contexte de test qui fournit
        ///des informations sur la série de tests active, ainsi que ses fonctionnalités.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Attributs de tests supplémentaires
        //
        // Vous pouvez utiliser les attributs supplémentaires suivants lorsque vous écrivez vos tests :
        //
        // Utilisez ClassInitialize pour exécuter du code avant d'exécuter le premier test de la classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Utilisez ClassCleanup pour exécuter du code une fois que tous les tests d'une classe ont été exécutés
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Utilisez TestInitialize pour exécuter du code avant d'exécuter chaque test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Utilisez TestCleanup pour exécuter du code après que chaque test a été exécuté
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion
        /// <summary>
        /// Test l'ajout d'un produit
        /// </summary>
        [TestMethod]
        public void TestProduit()
        {
            var Produit = new GestionCommande.model.Produit();
            Produit.Designation="Test";
            Produit.Prix = 20;
            var s = new GestionCommande.controleur.CommandeControleur();
            s.CreerProduit(Produit);
            var listProduit = s.GetProduits().Last();
            bool resultat = new bool();
            
                if (listProduit.Designation == Produit.Designation)
                {
                    resultat = true;
                }
                else
                {
                    resultat = false;
                }
            
            Assert.IsTrue(resultat, "L'ajout de Produit fonctionne");
            




        }
        /// <summary>
        /// Test l'ajout d'une commande
        /// </summary>
        [TestMethod]
        public void TestAjoutCommandeOk()
        {
            bool resultat;
            var s = new GestionCommande.controleur.CommandeControleur();
            var client = s.GetClients().Last();
            ICollection<GestionCommande.model.LigneCommande> lignesCommande = new Collection<GestionCommande.model.LigneCommande>();
            var ligneCmd = new GestionCommande.model.LigneCommande() { Produit = s.GetProduits().First(), Quantite = 2 };
            lignesCommande.Add(ligneCmd);
            s.CreerCommande(client, lignesCommande);
            var CommandeCheck = s.GetCommandes().Last();
            if (CommandeCheck.Client.Id == client.Id)
            {
                if (CommandeCheck.LignesCommande.First().CommandeRefId == lignesCommande.First().CommandeRefId)
                {
                    /*
                     * A partir d'ici on peut considerer que le niveau de test est sufisant dans ce contexte
                     */
                    resultat = true;
                
                }
                else
                {
                    resultat = false;
                }
            }
            else
            {
                resultat = false;
            }
            Assert.IsTrue(resultat,"L'ajout de commande fonctionne");
         }

        /// <summary>
        /// Test L'ajout d'un client
        /// </summary>
        [TestMethod]
        public void TestClient()
        {
            string nom = "Lambert";
            string prenom = "Jean";
            string mail = "jean.lambert@gmail.com";
            var Client = new GestionCommande.model.Client { Nom = nom, Prenom = prenom, Mail = mail };
            var s = new GestionCommande.controleur.CommandeControleur();
            s.CreerClient(Client);
            var listClient = s.GetClients().Last();
            bool resultat;
           
                if (listClient.Nom == Client.Nom)
                {
                    resultat = true;

                }
                else
                {
                    resultat = false;
                }
              
            

            Assert.IsTrue(resultat, "L'ajout de client fonctionne");




        }
    }
}
