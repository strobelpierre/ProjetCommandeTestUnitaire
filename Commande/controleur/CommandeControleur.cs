﻿using GestionCommande.dao;
using GestionCommande.model;
using GestionCommande;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;

namespace GestionCommande.controleur
{
  public  class CommandeControleur:Controleur
    {
        private ClientDao ClientDao { get; }
        private ProduitDao ProduitDao { get; }
        private CommandeDao CommandeDao { get; }

        public CommandeControleur()
        {
            this.ClientDao = new ClientDao();
            this.ProduitDao = new ProduitDao();
            this.CommandeDao = new CommandeDao();
        }
        /// <summary>
        /// Creer une fonction
        /// </summary>
        /// <param name="client">Objet Client</param>
        /// <param name="ligneCmd">Objet Ligne de commande</param>

        public void CreerCommande(Client client,ICollection<LigneCommande> ligneCmd)
        {
            GestionCommande.model.Commande cmd = new model.Commande { Id = CommandeDao.Commandes.Count + 1, Client = client, LignesCommande = ligneCmd };
            foreach (LigneCommande ligne in ligneCmd)
            {
                ligne.Commande = cmd;
            }
            client.Commandes.Add(cmd);
            CommandeDao.AjouterCommande(cmd);
        }
        /// <summary>
        /// Creer un client
        /// </summary>
        /// <param name="client">Objet Client</param>
        public void CreerClient(Client client)
        {
            Client cli = new Client { Id = ClientDao.Clients.Count + 1, Nom = client.Nom, Prenom = client.Prenom, Mail = client.Mail };
            try
            {

                ClientDao.AjouterClient(cli);
            }
            catch (ValidationException e)
            {
                Console.WriteLine(e);
                throw;
            }

           
        }
        /// <summary>
        /// Creer un produit
        /// </summary>
        /// <param name="produit">Objet Produit</param>
        public void CreerProduit(Produit produit)
        {
            Produit pro = new Produit { Id = ProduitDao.Produits.Count + 1, Designation = produit.Designation, Prix = produit.Prix };


            ProduitDao.Ajouter(pro);
        }
        /// <summary>
        /// Obtenir la liste des clients
        /// </summary>
        /// <returns>Collection de client via l'objet ClientDao.Clients</returns>
        public  ICollection<Client>  GetClients()
        {
            return ClientDao.Clients;
        }
        /// <summary>
        /// Obtenir la liste des produits
        /// </summary>
        /// <returns>Collection de produit via l'objet ProduitDao.Produits</returns>

        public ICollection<Produit> GetProduits()
        {
            return ProduitDao.Produits;
        }
        /// <summary>
        /// Obtenir la liste des commandes
        /// </summary>
        /// <returns>Collection de commande via l'objet CommandeDao.Commandes</returns>
        public ICollection<model.Commande> GetCommandes()
        {
            return CommandeDao.Commandes;
        }
       
        ICollection<GestionCommande.model.Commande> Controleur.GetCommandes()
        {
            throw new NotImplementedException();
        }
    }
}
