﻿using GestionCommande.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionCommande.controleur
{
    public interface Controleur
    {
  
        void CreerCommande(Client client, ICollection<LigneCommande> ligneCmd);
        void CreerClient(Client client);
        void CreerProduit(Produit produit);


        ICollection<Client> GetClients();

        ICollection<Produit> GetProduits();

        ICollection<model.Commande> GetCommandes();
    }
}
