﻿using GestionCommande.controleur;
using GestionCommande.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionCommande.vue
{
    class MainVue
    {
        private Controleur controleur;
        public MainVue(Controleur controleur)
        {
            this.controleur = controleur;
        }

        public void start()
        {
            string input = "";
            do
            {
                Console.WriteLine("Quelle actions souhaitez vous faire : ");
                Console.WriteLine("1 : Afficher la liste des clients");
                Console.WriteLine("2 : Afficher la liste des produits");
                Console.WriteLine("3 : Afficher la liste des commandes");
                Console.WriteLine("4 : Afficher les commandes d'un client");
                Console.WriteLine("5 : Ajouter une commande");
                Console.WriteLine("6 : Ajouter un client");
                Console.WriteLine("7 : Ajouter un produit");
                Console.WriteLine("q : quitter");

                input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        AfficherClients();
                        break;
                    case "2":
                        AfficherProduits();
                        break;
                    case "3":
                        AfficherCommandes();
                        break;
                    case "4":
                        AfficherCommandeParClient();
                        break;
                    case "5":
                        AjouterCommande();
                        break;
                    case "6":
                        AjouterClient();
                        break;
                    case "7":
                        CreateProduit();
                        break;
                    default:
                        break;
                }
                Console.WriteLine();
            } while (!"q".Equals(input));
        }
        /// <summary>
        /// Affiche les Commande Par Client
        /// </summary>
        private void AfficherCommandeParClient()
        {
            Client client = this.ChoisirClient();
            this.AfficherCommandes(client.Commandes);
        }
        /// <summary>
        /// Appel l'affichage de la liste de toutes les commandes
        /// </summary>
        private void AfficherCommandes()
        {
            this.AfficherCommandes(controleur.GetCommandes());
        }
        /// <summary>
        /// Gere l'affichage des commandes 
        /// </summary>
        /// <param name="commandes"></param>
        private void AfficherCommandes(ICollection<model.Commande> commandes)
        {
            foreach (model.Commande commande in commandes)
            {
                Console.WriteLine("Client : " + commande.Client.Prenom + " " + commande.Client.Nom);
                Console.WriteLine("Produits : ");
                foreach (LigneCommande ligneCommande in commande.LignesCommande)
                {
                    Console.WriteLine("Désignation : " + ligneCommande.Produit.Designation);
                    Console.WriteLine("Quantité : " + ligneCommande.Quantite);
                }
                Console.WriteLine();
            }
        }
        /// <summary>
        /// Gere l'affichage des produit
        /// </summary>
        private void AfficherProduits()
        {
           foreach(Produit produit in controleur.GetProduits())
            {
                Console.WriteLine("Désignation : " + produit.Designation);
                Console.WriteLine("Prix : "+produit.Prix);
            }
        }
        /// <summary>
        /// Interface d'ajout de commande fait appel au controleur pour la gestion de l'ajout de la commande
        /// </summary>
        private void AjouterCommande()
        {
            Console.WriteLine("Création d'une nouvelle commande");

            Client cli = this.ChoisirClient();
            ICollection<LigneCommande> lignesCommande = this.AjouterLignes();

            controleur.CreerCommande(cli, lignesCommande);
        }
        /// <summary>
        /// Interface de creation des produit fait appel au controleur pour la gestion de l'ajout du produit
        /// </summary>
        private void CreateProduit()
        {
            Console.WriteLine("Création d'un nouveau produit");

            Produit produit = new Produit();
        
            Console.WriteLine("Designation :");
            produit.Designation = Console.ReadLine();
            Console.WriteLine("Prix :");
            produit.Prix = int.Parse(Console.ReadLine());
          
            controleur.CreerProduit(produit);

           
        }
        /// <summary>
        /// Gestion de l'interface d'ajout de client, fait appel au controlleur pour gerer l'ajout du client
        /// </summary>
        private void AjouterClient()
        {
            Console.WriteLine("Création du nouveau client");

            Client cli = new Client();
            Console.WriteLine("Nom :");
            cli.Nom = Console.ReadLine();
            Console.WriteLine("Prenom :");
            cli.Prenom = Console.ReadLine();
            Console.WriteLine("Mail : ");
            cli.Mail = Console.ReadLine();
            controleur.CreerClient(cli);
           

            
        }
        /// <summary>
        /// Interface de selection d'un client a partir de son id
        /// </summary>
        /// <returns>GestionModel.Client Client</returns>
        private Client ChoisirClient()
        {
            Console.WriteLine("Entrez l'ID du client pour la commande : ");
            ICollection<Client> clients = controleur.GetClients();
            this.AfficherClients();
            string input = Console.ReadLine();
            return  clients.Where(c => c.Id == int.Parse(input)).First();
        }
        /// <summary>
        /// Interface d'ajout de ligne dans une commande
        /// </summary>
        /// <returns>Collection de ligne de commandes</returns>
        private ICollection<LigneCommande> AjouterLignes()
        {
            ICollection<LigneCommande> lignesCommande = new Collection<LigneCommande>();
            string input = "";
            do
            {

                Produit prod = this.AjouterProduit();
                Console.WriteLine("Veuillez entrer une quantité");
                int qte = int.Parse(Console.ReadLine());

                LigneCommande ligneCmd = new LigneCommande() { Produit = prod, Quantite = qte };
                lignesCommande.Add(ligneCmd);
                Console.WriteLine("Appuyer sur 'q' si vous souhaitez arrêter d'ajouter des produits, sinon, appuyez sur n'importe quelle touche");
                input = Console.ReadLine();
            } while (!"q".Equals(input));

            return lignesCommande;
        }
        /// <summary>
        /// Permet d'ajouter un produit à une commande
        /// </summary>
        /// <returns>retourne un produit</returns>
        private Produit AjouterProduit()
        {
            Console.WriteLine("Entrez l'ID d'un produit à ajouter à la commande : ");
            ICollection<Produit> produits = controleur.GetProduits();
            foreach (Produit produit in produits)
            {
                Console.WriteLine(produit.Id + " : " + produit.Designation);
            }
            string input = Console.ReadLine();
            return produits.Where(p => p.Id == int.Parse(input)).First();
        }
        /// <summary>
        /// Ajout d'une quantié à commande
        /// </summary>
        /// <returns>Int quantité</returns>
        private int AjouterQuantité()
        {
            Console.WriteLine("Veuillez entrer une quantité");
            return int.Parse(Console.ReadLine());
        }
        /// <summary>
        /// Affiche les clients par id prenom et nom
        /// </summary>
        private void AfficherClients()
        {
            foreach (Client client in controleur.GetClients())
            {
                Console.WriteLine(client.Id + " : " + client.Prenom + " " + client.Nom);
            }
        }
    }
}
